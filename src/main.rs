use actix_web::{get, App, HttpRequest, HttpResponse, HttpServer, Responder, middleware, web};
use actix_web::http::header;
use tera::Tera;
use log::debug;
use std::collections::HashMap;

const X_FORWARDED_FOR: &[u8] = b"x-forwarded-for";

fn get_ip_from_req(req: & HttpRequest) -> Option<String> {
    let mut realip_remote_addr = None;

    // load forwarded header
    for hdr in req.headers().get_all(&header::FORWARDED) {
        if let Ok(val) = hdr.to_str() {
            for pair in val.split(';') {
                for el in pair.split(',') {
                    let mut items = el.trim().splitn(2, '=');
                    if let Some(name) = items.next() {
                        if let Some(val) = items.next() {
                            match &name.to_lowercase() as &str {
                                "for" => {
                                    if realip_remote_addr.is_none() {
                                        realip_remote_addr = Some(val.trim());
                                    }
                                }
                                _ => {println!("{:?}", val)},
                            }
                        }
                    }
                }
            }
        }
    }
    // get remote_addraddr from socketaddr
    let remote_addr = req.peer_addr().map(|addr| format!("{}", addr.ip()));

    if realip_remote_addr.is_none() {
        if let Some(h) = req
            .headers()
            .get(&header::HeaderName::from_lowercase(X_FORWARDED_FOR).unwrap())
        {
            if let Ok(h) = h.to_str() {
                realip_remote_addr = h.split(',').next().map(|v| v.trim());
            }
        }
    }

    if realip_remote_addr.is_some() {
        Some(String::from(realip_remote_addr.unwrap()))
    } else {
        Some(String::from(remote_addr.unwrap()))
    }
}

#[get("/")]
async fn get_ip(tmpl: web::Data<tera::Tera>, req: HttpRequest) -> impl Responder {
    let mut ctx = tera::Context::new();
    let mut data = HashMap::new();

    for (k, v) in req.headers().iter() {
        data.insert(k.as_str(), v.to_str().unwrap());
    }
    let addr = get_ip_from_req(&req).unwrap();
    data.insert("ip", &addr);
    ctx.insert("data", &data);

    if let Some(accept) = req.headers().get("accept") {
        if accept.to_str().unwrap().contains("json") {
            debug!("Accept json");
            let body = tmpl.render("ip.json", &ctx).unwrap();
            return HttpResponse::Ok().content_type("text/json").body(body);
        } else if accept.to_str().unwrap().contains("html") {
            debug!("Accept html");
            let body = tmpl.render("ip.html", &ctx).unwrap();
            return HttpResponse::Ok().content_type("text/html").body(body);
        }
    }
    debug!("Accept not json nor html");

    let body = tmpl.render("ip_raw.html", &ctx).unwrap();
    HttpResponse::Ok().content_type("text/html").body(body)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    HttpServer::new(|| {
        let tera =
            Tera::new(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*")).unwrap();

        App::new()
            .data(tera)
            .wrap(middleware::Logger::default())
            .service(get_ip)
    })
    .bind("0.0.0.0:8008")?
    .workers(8)
    .run()
    .await
}
